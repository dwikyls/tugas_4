<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FilmController@index');
Route::get('/film/detail/{id}', 'FilmController@detail');
Route::get('/film/daftar/{id}', 'FilmController@daftar');

Route::post('/auth/login', 'AuthController@index');
Route::get('/auth/login', 'AuthController@login');
Route::get('/auth/logout', 'AuthController@logout');

Route::get('/user/profile', 'UserController@index');
Route::get('/user/registrasi', 'UserController@registrasi');
Route::post('/user/registrasi', 'UserController@insert');
Route::get('/user/edit', 'UserController@edit');
Route::post('/user/edit/{id}', 'UserController@update');
