@extends('templates/layout')

@section('title', $title)

@section('container')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block mt-3">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
            </div>
	    @endif

        <div class="row">
            <div class="col-2"><a href="/" class="btn btn-primary"><- Kembali</a></div>
            <div class="col-8 text-center"><h1>Detail</h1></div>
        </div>
        <div class="row">
            <div class="col-8 mb-4">
                <div class="card h-100">
                    <img src="{{ $IMG_URL.$movie['backdrop_path'] }}" class="card-img-top" alt="movie poster">
                    <div class="card-body">
                        <h5 class="card-title">{{ $movie['original_title'] }}</h5>
                        <p class="card-text">{{ $movie['overview'] }}</p>
                        <p class="card-text">Category: <strong>{{ $movie['adult'] === 'true' ? 'Adult' : 'Common' }}</strong></p>
                        <p class="card-text">
                            Genre: @foreach ($movie['genres'] as $genre)
                                <strong>{{ $genre['name'] }}, </strong>
                            @endforeach
                        </p>
                        <p class="card-text">Rating: <strong>{{ $movie['vote_average'] }}</strong></p>
                    </div>
                </div>
            </div>
            <div class="col-4 mb-4">
                <div class="card h-100">
                    <div class="card-body">
                        <h5 class="card-title">Informasi</h5>
                        <p class="card-text">Deadline <br><strong class="card-text text-danger">3 hari 4 jam lagi</strong></p>
                        <p class="card-text">Sisa Peserta <br><strong class="card-text text-danger">100</strong></p>
                        <p class="card-text">Jadwal <br><strong class="card-text">2 Februari 2020, <br>Pukul 20.00</strong></p>
                        <p class="card-text">Lokasi <br><strong class="card-text">Etopia Selatan No. 4</strong></p>
                        <p class="card-text">Biaya Administrasi<br><strong class="card-text text-success">Rp. 0</strong></p>
                    </div>
                    <div class="card-footer">
                        <div class="d-flex justify-content-around">
                        @if (isset($terdaftar))
                            <?= $terdaftar; ?>
                            <a href="" class="btn btn-secondary">Simpan</a>
                        @else
                            <a href="/" class="btn btn-primary">Daftar</a>
                            <a href="" class="btn btn-secondary">Simpan</a>
                        @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection