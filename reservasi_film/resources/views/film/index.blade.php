@extends('templates/layout')

@section('title', $title)

@section('container')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block mt-3">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
            </div>
	    @endif
        <div class="row mb-3">
            <div class="col">
                <h1>Popular</h1>
            </div>
        </div>
        <div class="row row-cols-1 row-cols-md-3">
            @foreach ($movies as $movie)
            <div class="col mb-4">
                <div class="card h-100">
                    <img src="{{ $IMG_URL.$movie['backdrop_path'] }}" class="card-img-top" alt="movie poster">
                    <div class="card-body">
                        <h5 class="card-title">
                            <a href="/film/detail/{{ $movie['id'] }}" class="">{{ $movie['original_title'] }}</a>
                        </h5>
                        <p class="card-text">{{ $movie['overview'] }}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

@endsection