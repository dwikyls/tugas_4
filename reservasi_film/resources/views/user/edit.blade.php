@extends('templates/layout')

@section('title', $title)

@section('container')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block mt-3">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
            </div>
	    @endif

        <div class="row">
            <div class="col-2"><a href="/user/profile" class="btn btn-primary"><- Kembali</a></div>
            <div class="col-8 text-center"><h1>{{ $title }}</h1></div>
        </div>
        <div class="row justify-items-center">
            <div class="col-5 text-right">
                <img src="{{url('/images/bg-edit.png')}}" alt="" style="width: 26rem;">
            </div>
            <div class="col-5 border p-5">
            <h1>{{ $title }}</h1>
                <form action="/user/edit/{{ $user->id }}" method="POST">
                @csrf
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama Lengkap" value="{{ old('nama') ? old('nama') : $user->nama }}">
                    </div>
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" name="email" class="form-control" id="email" placeholder="email@example.com" value="{{ old('email') ? old('email') : $user->email }}">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="text" name="password" class="form-control" id="password" placeholder="Passphrase" value="{{ old('password') ? old('password') : $user->password }}">
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Submit</button>
                </form>
            </div>
        </div>
    </div>

@endsection