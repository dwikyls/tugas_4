@extends('templates/layout')

@section('title', $title)

@section('container')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block mt-3">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
            </div>
	    @endif

        <div class="row">
            <div class="col-2 text-left"><a href="/" class="btn btn-primary"><- Kembali</a></div>
            <div class="col-8 text-center"><h1>Detail</h1></div>
            <div class="col-2 text-right"><a href="/user/edit" class="btn btn-secondary">Edit Profile</a></div>
        </div>
        <div class="row">
            <div class="col mb-4">
                <div class="card bg-transparent text-white">
                    <img src="{{url('/images/sampul-default.jpg')}}" class="card-img w-75" alt="">
                    <div class="card-img-overlay d-flex justify-content-center">
                        <div class="card bg-transparent border-0" style="width: 16rem; margin-top: 250px;">
                            <img src="{{url('/images/default.png')}}" class="card-img-top border" alt="">
                            <div class="card-body text-dark text-center">
                                <h5 class="card-title">{{ $nama }}</h5>
                                <p class="card-text">{{ $status }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row border p-3" style="margin-top: 220px;">
            <div class="container">
                <div class="row">
                    <div class="col text-center">
                        <h2>History Film</h2>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col mb-4">
                        <table class="table" aria-label="">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Judul Film</th>
                                    <th scope="col">Tanggal</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($movies as $movie)
                                <tr>
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ $movie->nama_film }}</td>
                                    <td>{{ $movie->created_at }}</td>
                                    <td>
                                        <a href="/film/detail/{{ $movie->id_film }}" class="badge badge-primary">Detail</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection