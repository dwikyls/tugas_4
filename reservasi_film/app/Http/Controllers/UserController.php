<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Session\Session;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class UserController extends BaseController
{
    protected $BASE_URL = 'https://api.themoviedb.org/3/';
    protected $API_KEY = 'e829ca6cc7cd28a8d775eb977c64dff8';

    public function index()
    {
        $email = session('email');

        $user = DB::table('user')->where('email', $email)->first();
        
        $id_user = $user->id;

        $movies = DB::table('order_film')->where('id_user', $id_user)->get();
        
        $data = [   
            'title' => 'My Profile',
            'nama' => $user->nama,
            'status' => $user->status,
            'movies' => $movies,
        ];

        return view('user/profile', $data);
    }

    public function registrasi()
    {
        $data = [
            'title' => 'Registrasi'
        ];

        return view('user/registrasi', $data);
    }

    public function insert(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
            'nama' => 'required'
        ]);

        DB::table('user')->insert([
            'email' => $request->email,
            'password' => $request->password,
            'nama' => $request->nama
        ]);

        return redirect('/')->with(['success' => 'Berhasil! Silahkan login terlebih dulu.']);
    }

    public function edit()
    {
        $email = session('email');

        $user = DB::table('user')->where('email', $email)->first();
        
        $data = [
            'title' => 'Edit Profile',
            'user' => $user
        ];

        return view('user/edit', $data);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
            'nama' => 'required'
        ]);

        DB::table('user')->where('id', $id)->update([
            'email' => $request->email,
            'password' => $request->password,
            'nama' => $request->nama
        ]);

        return redirect('/user/profile')->with(['success' => 'Berhasil!']);
    }
}
