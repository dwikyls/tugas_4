<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Session\Session;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;

class FilmController extends BaseController
{   
    protected $BASE_URL = 'https://api.themoviedb.org/3/';
    protected $IMG_URL = 'https://image.tmdb.org/t/p/w500/';
    protected $API_KEY = 'e829ca6cc7cd28a8d775eb977c64dff8';

    public function index()
    {   
        $POPULAR_ENDPOINT = $this->BASE_URL.'movie/popular?api_key='.$this->API_KEY;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $POPULAR_ENDPOINT);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        $movies = json_decode($result, true);

        $data = [
            'title' => 'okoc',
            'movies' => $movies['results'],
            'IMG_URL' => $this->IMG_URL
        ];

        return view('film/index', $data);
    }

    public function detail($id)
    {
        $DETAIL_ENDPOINT = $this->BASE_URL.'movie/'.$id.'?api_key='.$this->API_KEY;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $DETAIL_ENDPOINT);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        $movie = json_decode($result, true);

        if(session('email') !== null){
            $email = session('email');

            $user = DB::table('user')->where('email', $email)->first();
            
            $id_user = $user->id;
    
            $movies = DB::table('order_film')->where('id_user', $id_user)->get();

            $data = [
                'title' => 'Detail',
                'movie' => $movie,
                'IMG_URL' => $this->IMG_URL,
                'terdaftar' => '<a href="/film/daftar/'.$movie['id'].'" class="btn btn-primary">Daftar</a>'
            ];

            foreach ($movies as $film) {
                if($film->id_film === $movie['id']){
                    $data['terdaftar'] = '<a href="/user/profile" class="btn btn-transparent text-muted">Terdaftar</a>';
                }
            }

            return view('film/detail', $data);
        }

        $data = [
            'title' => 'Detail',
            'movie' => $movie,
            'IMG_URL' => $this->IMG_URL
        ];

        return view('film/detail', $data);
    }

    public function daftar($id)
    {   
        $email = session()->get('email');

        $user = DB::table('user')->where('email', $email)->get();

        $DETAIL_ENDPOINT = $this->BASE_URL.'movie/'.$id.'?api_key='.$this->API_KEY;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $DETAIL_ENDPOINT);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        $movie = json_decode($result, true);
        
        $id_film = $id;
        $nama_film = $movie['original_title'];
        $id_user = $user[0]->id;

        DB::table('order_film')->insert([
            'id_film' => $id_film,
            'nama_film' => $nama_film,
            'id_user' => $id_user
        ]);

        return back()->with(['success' => 'Berhasil!']);
    }
}
