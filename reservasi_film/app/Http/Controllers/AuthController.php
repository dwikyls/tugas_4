<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;

class AuthController extends BaseController
{

    public function index(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        $user = DB::table('user')->where('email', $email)->get();

        if(empty($user) === false && $user[0]->password === $password){
            $data = [
                'email' => $user[0]->email
            ];

            session($data);
        }
        
        return redirect('/');
    }

    public function login()
    {
        $data = [
          'title' => 'Login'  
        ];

        return view('/user/login', $data);
    }

    public function logout()
    {
        session()->flush();

        return redirect('/');
    }
}
