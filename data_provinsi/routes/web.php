<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Kabupaten
Route::get('/kabupaten', 'KabupatenController@index');
Route::get('/kabupaten/detail/{id}', 'KabupatenController@detail');

Route::get('/kabupaten/tambah/{id}', 'KabupatenController@tambah');
Route::post('/kabupaten/tambah', 'KabupatenController@submit');

Route::get('/kabupaten/edit/{id}', 'KabupatenController@edit');
Route::post('/kabupaten/edit/{id}', 'KabupatenController@update');

Route::get('/kabupaten/delete/{id}', 'KabupatenController@delete');

// Provinsi
Route::get('/', 'ProvinsiController@index');
Route::get('/provinsi', 'ProvinsiController@index');
Route::get('/provinsi/detail/{id}', 'ProvinsiController@detail');

Route::get('/provinsi/tambah', 'ProvinsiController@tambah');
Route::post('/provinsi/tambah', 'ProvinsiController@submit');

Route::get('/provinsi/edit/{id}', 'ProvinsiController@edit');
Route::post('/provinsi/edit/{id}', 'ProvinsiController@update');

Route::get('/provinsi/delete/{id}', 'ProvinsiController@delete');
