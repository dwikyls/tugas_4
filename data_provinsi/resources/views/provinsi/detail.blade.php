@extends('templates/layout')

@section('title', $title)

@section('container')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block mt-3">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
            </div>
	    @endif
        <div class="row">
            <div class="col">
                <h1>{{ $title }}</h1>
                <a href="/provinsi" class="btn btn-primary"><- Kembali</a>
                <a href="/kabupaten/tambah/{{ $id_provinsi }}" class="btn btn-primary">+ Tambah Data</a>
                <table class="table mt-3" aria-label="">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Kabupaten</th>
                            <th scope="col">Kecamatan</th>
                            <th scope="col">Desa</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($provinsis as $provinsi)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $provinsi->nama_kabupaten }}</td>
                            <td>{{ $provinsi->nama_kecamatan }}</td>
                            <td>{{ $provinsi->nama_desa }}</td>
                            <td>
                                <a href="/kabupaten/edit/{{ $provinsi->id }}" class="badge badge-secondary">Edit</a>
                                <a href="/kabupaten/delete/{{ $provinsi->id }}" class="badge badge-danger">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection