@extends('templates/layout')

@section('title', $title)

@section('container')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block mt-3">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
            </div>
	    @endif
        <div class="row">
            <div class="col">
                <h1>{{ $title }}</h1>
                <a href="/" class="btn btn-primary"><- Kembali</a>
                <form action="/provinsi/edit/{{ $provinsi[0]->id }}" method="POST">
                @csrf
                    <div class="form-group">
                        <label for="nama_provinsi">Nama Provinsi</label>
                        <input type="text" name="nama_provinsi" class="form-control" id="nama_provinsi" value="{{ $provinsi[0]->nama_provinsi }}">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

@endsection