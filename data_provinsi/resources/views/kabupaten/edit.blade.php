@extends('templates/layout')

@section('title', $title)

@section('container')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block mt-3">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
            </div>
	    @endif
        <div class="row">
            <div class="col">
                <h1>{{ $title }}</h1>
                <a href="/provinsi/detail/{{ $kabupaten[0]->id_provinsi }}" class="btn btn-primary"><- Kembali</a>
                <form action="/kabupaten/edit/{{ $kabupaten[0]->id }}" method="POST">
                @csrf
                    <input type="hidden" name="id_provinsi" value="{{ $kabupaten[0]->id_provinsi }}">
                    <div class="form-group">
                        <label for="nama_desa">Nama Desa</label>
                        <input type="text" name="nama_desa" class="form-control" id="nama_desa" value="{{ $kabupaten[0]->nama_desa }}">
                    </div>
                    <div class="form-group">
                        <label for="nama_kecamatan">Nama Kecamatan</label>
                        <input type="text" name="nama_kecamatan" class="form-control" id="nama_kecamatan" value="{{ $kabupaten[0]->nama_kecamatan }}">
                    </div>
                    <div class="form-group">
                        <label for="nama_kabupaten">Nama Kabupaten</label>
                        <input type="text" name="nama_kabupaten" class="form-control" id="nama_kabupaten" value="{{ $kabupaten[0]->nama_kabupaten }}">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

@endsection