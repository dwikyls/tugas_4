@extends('templates/layout')

@section('title', $title)

@section('container')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block mt-3">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
            </div>
	    @endif
        <div class="row">
            <div class="col-8">
                <h1>{{ $title }}</h1>
                <a href="/provinsi/detail/{{ $id_provinsi }}" class="btn btn-primary"><- Kembali</a>
                <form action="/kabupaten/tambah" method="POST">
                @csrf
                    <div class="form-group">
                        <label for="nama_desa">Nama Desa</label>
                        <input type="text" name="nama_desa" class="form-control" id="nama_desa" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                        <label for="nama_kecamatan">Nama Kecamatan</label>
                        <input type="text" name="nama_kecamatan" class="form-control" id="nama_kecamatan">
                    </div>
                    <div class="form-group">
                        <label for="nama_kabupaten">Nama Kabupaten</label>
                        <input type="text" name="nama_kabupaten" class="form-control" id="nama_kabupaten">
                    </div>
                    <input type="hidden" name="id_provinsi" value="{{ $id_provinsi }}">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

@endsection