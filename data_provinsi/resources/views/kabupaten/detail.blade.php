@extends('templates/layout')

@section('title', $title)

@section('container')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block mt-3">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
            </div>
	    @endif
        <div class="row">
            <div class="col">
                <a href="/" class="btn btn-primary"><- Kembali</a>
                <h1>{{ $title }}</h1>
                <div class="card" style="width: 18rem;">
                    <div class="card-body">
                        <p class="card-text">Desa: <strong>{{ $desa[0]->nama_desa }}</strong></p>
                        <p class="card-text">Kecamatan: <strong>{{ $desa[0]->nama_kecamatan }}</strong></p>
                        <p class="card-text">Kabupaten: <strong>{{ $desa[0]->nama_kabupaten }}</strong></p>
                        <p class="card-text">Provinsi: <strong>{{ $desa[0]->nama_provinsi }}</strong></p>
                        <a href="/edit/{{ $desa[0]->id }}" class="btn btn-primary">Edit</a>
                        <a href="/delete/{{ $desa[0]->id }}" class="btn btn-danger">Delete</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection