@extends('templates/layout')

@section('title', $title)

@section('container')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block mt-3">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
            </div>
	    @endif
        <div class="row">
            <div class="col-8">
                <h1>{{ $title }}</h1>
                <a href="/tambah" class="btn btn-primary">+ Tambah Data</a>
                <table class="table mt-3" aria-label="">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Kabupaten</th>
                            <th scope="col">Provinsi</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($kabupatens as $kabupaten)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $kabupaten->nama_kabupaten }}</td>
                            <td>{{ $kabupaten->nama_provinsi }}</td>
                            <td>
                                <a href="/kabupaten/detail/{{ $kabupaten->id_kabupaten }}" class="badge badge-primary">Detail</a>
                                <a href="/kabupaten/edit/{{ $kabupaten->id_kabupaten }}" class="badge badge-secondary">Edit</a>
                                <a href="/kabupaten/delete/{{ $kabupaten->id_kabupaten }}" class="badge badge-danger">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection