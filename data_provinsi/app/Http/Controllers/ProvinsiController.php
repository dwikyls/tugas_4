<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;

class ProvinsiController extends BaseController
{
    public function index()
    {
        $provinsis = DB::table('provinsi')->get();

        $data = [
            'title' => 'Data Provinsi',
            'provinsis' => $provinsis
        ];

        return view('provinsi/index', $data);
    }

    public function detail($id)
    {   
        $provinsis = DB::table('kabupaten')
                    ->where('id_provinsi', $id)
                    ->get();
        
        $data = [
            'title' => 'Detail Provinsi',
            'provinsis' => $provinsis,
            'id_provinsi' => $id
        ];

        return view('provinsi/detail', $data);
    }

    public function tambah()
    {
        $data = [
            'title' => 'Tambah Data'
        ];

        return view('provinsi/tambah', $data);
    }

    public function submit(Request $request)
    {
        DB::table('provinsi')->insert([
            'nama_provinsi' => $request->nama_provinsi
        ]);

        return redirect('/')->with(['success' => 'Data berhasil ditambah!']);
    }

    public function edit($id)
    {
        $provinsi = DB::table('provinsi')
                    ->where('id', $id)
                    ->get();
        
        $data = [
            'title' => 'Edit Data',
            'provinsi' => $provinsi
        ];

        return view('provinsi/edit', $data);
    }

    public function update(Request $request, $id)
    {
        DB::table('provinsi')->where('id', $id)->update([
            'nama_provinsi' => $request->nama_provinsi
        ]);

        return redirect('/')->with(['success' => 'Data berhasil diubah!']);
    }

    public function delete($id)
    {
        DB::table('provinsi')->where('id', $id)->delete();
        
        return redirect('/')->with(['success' => 'Data berhasil dihapus!']);
    }
}
