<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;

class KabupatenController extends BaseController
{
    public function index()
    {
        $kabupatens = DB::table('kabupaten')
                        ->join('provinsi', 'kabupaten.id_provinsi', '=', 'provinsi.id')
                        ->select('kabupaten.id as id_kabupaten', 'kabupaten.nama_kabupaten', 'provinsi.nama_provinsi')
                        ->get();
        // dd($kabupatens);
        $data = [
            'title' => 'Data Provinsi',
            'kabupatens' => $kabupatens
        ];

        return view('kabupaten/index', $data);
    }

    public function detail($id)
    {   
        $provinsis = DB::table('kabupaten')
                    ->where('id_provinsi', $id)
                    ->get();

        $data = [
            'title' => 'Detail Provinsi',
            'provinsis' => $provinsis,
            'id_provinsi' => $id
        ];

        return view('provinsi/detail', $data);
    }

    public function tambah($id)
    {
        $data = [
            'title' => 'Tambah Data',
            'id_provinsi' => $id
        ];

        return view('kabupaten/tambah', $data);
    }

    public function submit(Request $request)
    {
        DB::table('kabupaten')->insert([
            'nama_desa' => $request->nama_desa,
            'nama_kecamatan' => $request->nama_kecamatan,
            'nama_kabupaten' => $request->nama_kabupaten,
            'id_provinsi' => $request->id_provinsi
        ]);

        return back()->with(['success' => 'Data berhasil ditambah!']);
    }

    public function edit($id)
    {
        $kabupaten = DB::table('kabupaten')
                    ->where('id', $id)
                    ->get();

        $data = [
            'title' => 'Edit',
            'kabupaten' => $kabupaten
        ];

        return view('kabupaten/edit', $data);
    }

    public function update(Request $request, $id)
    {
        DB::table('kabupaten')->where('id', $id)->update([
            'nama_desa' => $request->nama_desa,
            'nama_kecamatan' => $request->nama_kecamatan,
            'nama_kabupaten' => $request->nama_kabupaten
        ]);

        return redirect('provinsi/detail/'.$request->id_provinsi)->with(['success' => 'Data berhasil diubah!']);
    }

    public function delete($id)
    {
        DB::table('kabupaten')->where('id', $id)->delete();
        
        return back()->with(['success' => 'Data berhasil dihapus!']);
    }
}
