<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\DB;

class EmployeeExport implements FromView
{
    public function view(): View
    {
        $excel_data = DB::table('employee')
                        ->select('employee.id', 'employee.nama', 
                                    DB::raw('CASE
                                    WHEN ISNULL(atasan_id) THEN "CEO"
                                    WHEN employee.atasan_id IN (
                                        SELECT id FROM employee
                                        WHERE isnull(atasan_id)) THEN "Direktur"
                                    WHEN employee.atasan_id IN (
                                        SELECT id FROM employee
                                        WHERE atasan_id IN (
                                            SELECT id FROM employee
                                            WHERE isnull(atasan_id))) THEN "Manajer"
                                    ELSE "Staff"
                                    END AS Posisi, company.nama AS Perusahaan')
                                )
                        ->join('company', 'employee.company_id', '=', 'company.id')->get();

        return view('templates/table', [
            'employees' => $excel_data
        ]);
    }
}