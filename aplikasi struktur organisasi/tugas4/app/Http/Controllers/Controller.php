<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use App\Student;

class Controller extends BaseController
{
    public function index()
    {
        $students = Student::all();

        return view('index', ['students' => $students]);
    }
}
