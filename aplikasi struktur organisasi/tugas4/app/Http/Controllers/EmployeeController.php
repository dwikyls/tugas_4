<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Excel;
use App\Exports\EmployeeExport;

class EmployeeController extends BaseController
{
    public function index()
    {
        $employees = DB::table('employee')->get();

        $data = [
            'employees' => $employees
        ];

        return view('employee/index', $data);
    }

    public function exportView()
    {
        $employees = DB::table('employee')
                        ->select('employee.id', 'employee.nama', 
                                    DB::raw('CASE
                                    WHEN ISNULL(atasan_id) THEN "CEO"
                                    WHEN employee.atasan_id IN (
                                        SELECT id FROM employee
                                        WHERE isnull(atasan_id)) THEN "Direktur"
                                    WHEN employee.atasan_id IN (
                                        SELECT id FROM employee
                                        WHERE atasan_id IN (
                                            SELECT id FROM employee
                                            WHERE isnull(atasan_id))) THEN "Manajer"
                                    ELSE "Staff"
                                    END AS Posisi, company.nama AS Perusahaan')
                                )
                        ->join('company', 'employee.company_id', '=', 'company.id')->get();
                        
        return view('employee/excel-view', compact('employees'));
    }

    public function exportExcel()
    {
        $nama_file = 'laporan_employee_'.date('Y-m-d_H-i-s').'.xlsx';
        return Excel::download(new EmployeeExport, $nama_file);
    }

    public function exportPdf()
    {
        $nama_file = 'laporan_employee_'.date('Y-m-d_H-i-s').'.pdf';
        return Excel::download(new EmployeeExport, $nama_file);
    }

    public function tambah()
    {
        return view('employee/tambah');
    }

    public function submit(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'company_id' => 'required'
        ]);

        DB::table('employee')->insert(
            [
                'nama' => $request->nama,
                'atasan_id' => $request->atasan_id,
                'company_id' => $request->company_id
            ]
        );

        return redirect('/')->with(['success' => 'Data berhasil ditambahkan!']);
    }

    public function edit($id)
    {
        $employee = DB::table('employee')->where('id', $id)->get();

        $data = [
            'employee' => $employee
        ];

        return view('employee/edit', $data);
    }

    public function update(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'company_id' => 'required'
        ]);

        DB::table('employee')->where('id', $request->id)->update(
            [
                'nama' => $request->nama,
                'atasan_id' => $request->atasan_id,
                'company_id' => $request->company_id
            ]
        );

        return back()->with(['success' => 'Data berhasil diupdate!']);
    }

    public function delete($id)
    {
        DB::table('employee')->where('id', $id)->delete();

        return redirect('/')->with(['success' => 'Data berhasil dihapus!']);
    }

    public function detail($id)
    {
        $employee = DB::table('employee')
                        ->where('employee.id', $id)
                        ->join('company', 'employee.company_id', '=', 'company.id')
                        ->select('employee.*', 'company.nama as company')
                        ->get();
        
        if($employee[0]->atasan_id === 1){
            $employee[0]->atasan_id = 'Direktur';
        } else if($employee[0]->atasan_id === 2 || $employee[0]->atasan_id === 3){
            $employee[0]->atasan_id = 'Manajer';
        } else if($employee[0]->atasan_id === 4 || $employee[0]->atasan_id === 5){
            $employee[0]->atasan_id = 'Staff';
        } else{
            $employee[0]->atasan_id = 'CEO';
        }

        return view('employee/detail', ['employee' => $employee]);
    }
}
