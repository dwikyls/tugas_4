<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@index');
Route::get('/employee', 'EmployeeController@index');
Route::get('/tambah', 'EmployeeController@tambah');
Route::post('/tambah', 'EmployeeController@submit');
Route::get('/edit/{id}', 'EmployeeController@edit');
Route::post('/edit', 'EmployeeController@update');
Route::get('/delete/{id}', 'EmployeeController@delete');
Route::get('/detail/{id}', 'EmployeeController@detail');

Route::get('/export-view', 'EmployeeController@exportView');
Route::get('/export-excel', 'EmployeeController@exportExcel');
Route::get('/export-pdf', 'EmployeeController@exportPdf');

Route::get('/company', 'CompanyController@index');
Route::get('/company/tambah', 'CompanyController@tambah');
Route::post('/company/tambah', 'CompanyController@submit');
Route::get('/company/detail/{id}', 'CompanyController@detail');
Route::get('/company/edit/{id}', 'CompanyController@edit');
Route::post('/company/edit', 'CompanyController@update');
Route::get('/company/delete/{id}', 'CompanyController@delete');
