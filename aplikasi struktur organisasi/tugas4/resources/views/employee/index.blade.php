@extends('templates/layout')

@section('container')

<div class="container">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block mt-3">
            <button type="button" class="btn-close" aria-label="Close"></button>	
            <strong>{{ $message }}</strong>
        </div>
    @endif

    <div class="row">
        <div class="col text-start">
            <a href="/tambah" class="btn btn-success">+ Tambah Data</a>
        </div>
        <div class="col text-center">
            <h1>Employee Table</h1>
        </div>
        <div class="col text-end">
            <a href="/export-view" class="btn btn-success">Export Data</a>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <table class="table text-center" aria-label="">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($employees as $employee)
                    <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{{ $employee->nama }}</td>
                        <td><a href="/detail/{{ $employee->id }}" class="badge bg-primary">Detail</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection