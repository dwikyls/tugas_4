@extends('templates/layout')

@section('container')

<div class="container">
    <div class="row">
        <div class="col">
            <a href="/" class="btn btn-success"><- Kembali</a>
            <h1>Tambah Data</h1>
            <form action="/tambah" method="POST">
            @csrf
                <div class="mb-3">
                    <label for="nama" class="form-label">Nama</label>
                    <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama Lengkap">
                </div>
                <div class="mb-3">
                    <label for="atasan_id" class="form-label">Atasan Id</label>
                    <input type="text" name="atasan_id" class="form-control" id="atasan_id" placeholder="1 ~ 4 atau kosongi untuk CEO">
                </div>
                <div class="mb-3">
                    <label for="company_id" class="form-label">Company Id</label>
                    <input type="text" name="company_id" class="form-control" id="company_id" placeholder="1 ~ 2">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>

@endsection