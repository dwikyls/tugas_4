@extends('templates/layout')

@section('container')

<div class="container">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block mt-3">
            <button type="button" class="btn-close" aria-label="Close"></button>	
            <strong>{{ $message }}</strong>
        </div>
    @endif

    <div class="row">
        <div class="col">
            <a href="/" class="btn btn-secondary"><- Kembali</a>
            <div class="card mt-4" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">Detail</h5>
                    <p class="card-text">Nama: <strong>{{ $employee[0]->nama }}</strong></p>
                    <p class="card-text">Jabatan: <strong>{{ $employee[0]->atasan_id }}</strong></p>
                    <p class="card-text">Company: <strong>{{ $employee[0]->company }}</strong></p>
                    <a href="/edit/{{ $employee[0]->id }}" class="btn btn-success">Edit</a>
                    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#exampleModal">
                        Delete
                    </button>

                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <p>Yakin ingin menghapus?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                    <a href="/delete/{{ $employee[0]->id }}" class="btn btn-danger">Delete</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection