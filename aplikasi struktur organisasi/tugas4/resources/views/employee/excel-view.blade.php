@extends('templates/layout')

@section('container')

<div class="container">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block mt-3">
            <button type="button" class="btn-close" aria-label="Close"></button>	
            <strong>{{ $message }}</strong>
        </div>
    @endif

    <div class="row">
        <div class="col text-start">
            <a href="/employee" class="btn btn-success"><- Kembali</a>
        </div>
        <div class="col text-center">
            <h1>Excel Data</h1>
        </div>
        <div class="col text-end">
            <a href="/export-excel" class="btn btn-success">Export to Excel</a>
            <a href="/export-pdf" class="btn btn-success">Export to PDF</a>
        </div>
    </div>

    <div class="row">
        <div class="col border">
            @include('templates/table', $employees)
        </div>
    </div>
</div>

@endsection