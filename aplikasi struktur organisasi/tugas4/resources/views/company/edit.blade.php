@extends('templates/layout')

@section('container')

<div class="container">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block mt-3">
            <button type="button" class="btn-close" aria-label="Close"></button>	
            <strong>{{ $message }}</strong>
        </div>
    @endif
    
    <div class="row">
        <div class="col">
            <a href="/company/detail/{{ $company[0]->id }}" class="btn btn-secondary"><- Kembali</a>
            <h1>Edit Data</h1>
            <form action="/company/edit" method="POST">
            @csrf
                <input type="hidden" name="id" value="{{ $company[0]->id }}">
                <div class="mb-3">
                    <label for="nama" class="form-label">Nama Company</label>
                    <input type="text" name="nama" class="form-control" id="nama" value="{{ $company[0]->nama }}">
                </div>
                <div class="mb-3">
                    <label for="alamat" class="form-label">Alamat</label>
                    <input type="text" name="alamat" class="form-control" id="alamat" value="{{ $company[0]->alamat }}">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>

@endsection