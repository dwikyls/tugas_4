@extends('templates/layout')

@section('container')

<div class="container">
    <div class="row">
        <div class="col">
            <a href="/company" class="btn btn-success"><- Kembali</a>
            <h1>Tambah Data</h1>
            <form action="/company/tambah" method="POST">
            @csrf
                <div class="mb-3">
                    <label for="nama" class="form-label">Nama Company</label>
                    <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama Company Lengkap">
                </div>
                <div class="mb-3">
                    <label for="alamat" class="form-label">Alamat</label>
                    <input type="text" name="alamat" class="form-control" id="alamat" placeholder="Alamat Company">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>

@endsection