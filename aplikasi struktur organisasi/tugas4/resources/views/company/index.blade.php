@extends('templates/layout')

@section('container')

<div class="container">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block mt-3">
            <button type="button" class="btn-close" aria-label="Close"></button>	
            <strong>{{ $message }}</strong>
        </div>
    @endif

    <div class="row">
        <div class="col">
            <a href="/company/tambah" class="btn btn-success">+ Tambah Data</a>
            <table class="table text-center" aria-label="">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($companies as $company)
                    <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{{ $company->nama }}</td>
                        <td><a href="/company/detail/{{ $company->id }}" class="badge bg-primary">Detail</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection