<table class="table text-center" aria-label="">
    <thead>
        <tr>
        <th scope="col">id</th>
        <th scope="col">Nama</th>
        <th scope="col">Posisi</th>
        <th scope="col">Perusahaan</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($employees as $employee)
        <tr>
            <th scope="row">{{ $employee->id }}</th>
            <td>{{ $employee->nama }}</td>
            <td>{{ $employee->Posisi }}</td>
            <td>{{ $employee->Perusahaan }}</td>
        </tr>
        @endforeach
    </tbody>
</table>